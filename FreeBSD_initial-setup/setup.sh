#!/usr/bin/env sh
# SPDX-License-Identifier: Unlicense
# SPDX-FileCopyrightText: 2023 Vieno Hakkerinen <txt.file@txtfile.eu>

## FreeBSD initial setup script
## written for initial setup of a FreeBSD machine which was installed using
## an image file (e.g. arm/aarch64 machines).

### get information
## default values
hostname='hostname'
wifi_SSID='wifi_SSID'
wifi_psk='12345678'

## get actual values via command line interface
printf '%s' 'hostname? '
read -r hostname
printf '%s' 'WiFi SSID? ' #TODO: what if ethernet should be used?
read -r wifi_SSID
printf '%s' 'WiFi password? '
read -r wifi_psk #TODO: check if ≥8 and ≤63 characters

### set stuff

# set hostname
sysrc hostname="${hostname}"

## set WiFi
# get WiFi devices
# TODO: probably broken if there is more than one WiFi device
dev=$(sysctl net.wlan.devices | cut -d: -f2 | tr -d ' ')

# /etc/rc.conf
sysrc "wlans_${dev}"="wlan0"
sysrc ifconfig_wlan0="WPA DHCP"
sysrc create_args_wlan0="country DE regdomain etsi"

# /etc/wpa_supplicant.conf
wpa_passphrase ${wifi_SSID} ${wifi_psk} > /etc/wpa_supplicant.conf

# setup DateTime stuff
# nptd panics if time differs to much
# run ntpdate for initial time sync and ntpd for continuous time sync
service ntpdate enable
service ntpd enable
# set timezone
tzsetup

# end
printf '%s' 'You changed hostname and network management. Best is to reboot.'
