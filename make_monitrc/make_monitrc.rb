﻿#!/usr/bin/env ruby
# frozen_string_literal: true

# SPDX-License-Identifier: Unlicense
# SPDX-FileCopyrightText: 2023 Vieno Hakkerinen <txt.file@txtfile.eu>

require 'tomlrb'

# copied from
# https://mmonit.com/monit/documentation/monit.html#CONNECTION-TESTS
protocols = %w( APACHE-STATUS DNS DWP FAIL2BAN FTP GPS HTTP HTTPS IMAP IMAPS CLAMAV LDAP2 LDAP3 LMTP MEMCACHE MONGODB MQTT MYSQL MYSQLS NNTP NTP3 PGSQL POP POPS POSTFIX-POLICY RADIUS RDATE REDIS RSYNC SIEVE SIP SMTP SMTPS SPAMASSASSIN SSH TNS WEBSOCKET )

conf = Tomlrb.load_file('config.toml', symbolize_keys: true)

conf.each_pair do |host, details|
	puts "check host #{host} with address #{host}"
	puts 'if failed ping6 timeout 2 seconds then alert'

	protocols.each do |protocol|
		protocol = protocol.downcase.to_sym

		if details.has_key?(protocol)

			# TODO: Currently port MUST be provided as array in
			# toml file.
			# Would be good to allow `ssh = true` in toml and then
			# use protocols standard port.
			# Would be good to allow `ssh = 22` in toml instead of
			# requiring the array.
			details[protocol].each do |port|
				puts "if failed port #{port} ipv6 protocol #{protocol} timeout 2 seconds then alert"
				if protocol == :https
					puts "if failed port #{port} ipv6 protocol #{protocol} certificate valid > 7 days then alert"
				end
			end
		end
	end
	puts ''
end
