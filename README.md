﻿<!-- SPDX-License-Identifier: Unlicense -->
<!-- SPDX-FileCopyrightText: 2023 Vieno Hakkerinen <txt.file@txtfile.eu> -->

# gist

Collection of small scripts and stuff.

## Licenses

All files have a proper [REUSE](https://reuse.software/) license & copyright header.
