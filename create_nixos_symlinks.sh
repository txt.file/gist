#!/usr/bin/env sh
#SPDX: CC0-1.0

# Create symlinks for NixOS system configuration and
# home-manager.
# The folder structure is one subfolder for each configured computer with that
# computers hostname. Below that hostname folder is sysconf/ and home-manager/.
# The former is for the system wide configuration in /etc/nixos/. The latter
# for the users configuration in ~/.config/nixpkgs/
# $(hostname)/{sysconf,home-manager}

# In POSIX sh, variable HOSTNAME is undefined
if [ -z "${HOSTNAME}" ]; then
	HOSTNAME=$(hostname)
fi

# NixOS system configuration
CONFIGPATH="/etc/nixos"
VCSPATH="${PWD}/${HOSTNAME}"
VCSPATHSYS="${VCSPATH}/sysconf"
if [ -d "${VCSPATHSYS}" ] && PRIVCMD=$(command -v doas) || PRIVCMD=$(command -v sudo); then
	for FILENAME in "${VCSPATHSYS}"/*; do
		FILENAME=${FILENAME##*/}
		if [ -f "$CONFIGPATH/$FILENAME" ]; then
			${PRIVCMD} rm "${CONFIGPATH}/${FILENAME}"
		elif [ -h "${CONFIGPATH}/${FILENAME}" ]; then
			${PRIVCMD} unlink "${CONFIGPATH}/${FILENAME}"
		fi
		${PRIVCMD} ln -s "${VCSPATHSYS}/${FILENAME}" "${CONFIGPATH}/${FILENAME}"
	done
elif ! command -v doas > /dev/null && ! command -v sudo > /dev/null; then
	printf "This script uses doas or sudo to gain elevated privileges.\n"
	printf "Neither doas nor sudo found on the system.\n" >&2
	printf "Not setting symlinks in %s.\n" "${CONFIGPATH}" >&2
fi

# configuration of home-manager for NixOS
CONFIGPATH="${HOME}/.config/nixpkgs"
VCSPATHHOME="${VCSPATH}/home-manager"
if [ -d "${VCSPATHHOME}" ]; then
	unlink "${CONFIGPATH}/home.nix"
	ln -s "${VCSPATHHOME}/${USER}/home.nix" "${CONFIGPATH}/home.nix"
fi
